import View from './view';

class RingFighterView extends View {
    constructor(className) {
        super();

        this.element = this.createRing(className);
    }

    createRing(className) {

        const element = this.createElement({ tagName: 'div', className: className, attributes: {} });
        this.nameElement = this.createElement({ tagName: 'h3', className: 'ring-fighter-name', attributes: {} });
        this.healthbarElement = this.createElement({ tagName: 'div', className: 'ring-fighter-healthbar', attributes: {} });
        const healthbarWrapper = this.createElement({ tagName: 'div', className: 'ring-fighter-healthbar-wrapper', attributes: {} });
        healthbarWrapper.append(this.healthbarElement);
        this.imageElement = this.createElement({ tagName: 'img', className: 'ring-fighter-image', attributes: {} });

        element.append(this.nameElement, healthbarWrapper, this.imageElement);

        return element;
    }

    setupFighter(fighter) {
        this.nameElement.innerText = fighter.name;
        this.maximumHealth = fighter.health;
        this.healthbarElement.style.width = '100%';
        this.imageElement.setAttribute('src', fighter.source);
    }

    updateHealthbar(health) {
        const currentPercents = Math.round(health * 100 / this.maximumHealth);
        this.healthbarElement.style.width = `${currentPercents}%`;
    }
}

export default RingFighterView;