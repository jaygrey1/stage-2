import View from './view';
import { fighterService } from './services/fightersService';

class DetailView extends View {
    fighter;

    constructor() {
        super();
        this.element = document.getElementById("fighter-info");
        this.form = document.getElementById("fi-content-form");

        document.getElementsByClassName("close")[0].addEventListener("click", this.crossHandler.bind(this), false);
        document.getElementById("fi-ok").addEventListener("click", this.okHandler.bind(this), false);
        document.getElementById("fi-cancel").addEventListener("click", this.cancelHandler.bind(this), false);
        document.getElementById("fi-update").addEventListener("click", this.updateHandler.bind(this), false);
    }

    createElement() {
        this.createElement({ tagName: "div", className: 'fi-content' })
            .appendChild(this.createElement({ tagName: "div", className: 'fi-header' }));
    }

    show(fighter) {
        this.fighter = fighter;

        this.form.elements.name.value = this.fighter.name;
        this.form.elements.health.value = this.fighter.health;
        this.form.elements.attack.value = this.fighter.attack;
        this.form.elements.defense.value = this.fighter.defense;

        this.element.style.display = "block";
    }

    hide() {
        this.element.style.display = "none";
    }

    crossHandler() {
        console.log("pressed window cross");
        this.hide();
    }

    okHandler() {
        console.log("pressed OK button");
        this.hide();
    }

    cancelHandler() {
        console.log("pressed Cancel button");
        this.hide();
    }

    updateHandler() {

        fighterService.updateFighterDetail(
            {
                id: this.fighter.id,
                name: this.form.elements.name.value,
                health: this.form.elements.health.value,
                attack: this.form.elements.attack.value,
                defense: this.form.elements.attack.value,
                source: this.fighter.source
            }
        ).then(result => {            
            return result == true ? fighterService.getFighterDetails(this.fighter.id) : Promise.reject(Error("fighter data not updated"));
        })
        .then(fighter => {
                const nameElement = document.querySelector(`div.fighter[data-fighter-id="${fighter.id}"] > span`);
                if (nameElement != null) {                    
                    nameElement.innerText = fighter.name;
                }
                console.log("fighter succesfully updated");
        })
        .finally(() => this.hide());
    }
}

export default DetailView;