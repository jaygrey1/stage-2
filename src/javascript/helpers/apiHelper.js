// const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const API_URL = 'https://lit-stream-86294.herokuapp.com/';


function callApi(endpoind, method) {
    const url = API_URL + endpoind;
    const options = {
        method
    };

    return fetch(url, options)
        .then(response =>
            response.ok
                ? response.json()
                : Promise.reject(Error('Failed to load'))
        )
        .catch(error => { throw error });
}

function encodeObject(object) {
    return encodeURI(Object.entries(object).map(e => e.join('=')).join('&'));
}

function callApiUpdate(endpoind, data) {
    const url = API_URL + endpoind;

    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    const params = {
        method: 'PUT',
        body: encodeObject(data),
        headers: headers
    };
        
    return fetch(url, params)
        .then(response => response.status == 204)
        .catch(error => {
            console.error(error);
            return false;
        });
    
}

export { callApi, callApiUpdate }