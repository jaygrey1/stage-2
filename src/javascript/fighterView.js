import View from './view';

class FighterView extends View {
    constructor(fighter, handleClick, handleSelectClick) {
        super();

        this.createFighter(fighter, handleClick, handleSelectClick);
    }

    createFighter(fighter, handleClick, handleSelectClick) {
        const { name, source } = fighter;
        const nameElement = this.createName(name);
        const imageElement = this.createImage(source);
        const checkboxElement = this.createCheckbox();

        this.element = this.createElement({ tagName: 'div', className: 'fighter', attributes: { 'data-fighter-id': fighter.id } });
        this.element.append(imageElement, nameElement, checkboxElement);
        this.element.addEventListener('click', event => handleClick(event, fighter), false);
        checkboxElement.addEventListener('click', event => handleSelectClick(event, fighter, this), false);

    }

    createName(name) {
        const nameElement = this.createElement({ tagName: 'span', className: 'name' });
        nameElement.innerText = name;

        return nameElement;
    }

    createImage(source) {
        const attributes = { src: source };
        const imgElement = this.createElement({ tagName: 'img', className: 'fighter-image', attributes });

        return imgElement;
    }

    createCheckbox() {
        const checkbox = this.createElement({ tagName: 'input', className: 'fighter-checkbox', attributes: { type: "checkbox" } });

        return checkbox;
    }

    select() {
        this.element.classList.add('fighter-selected');
    }

    deselect() {
        this.element.classList.remove('fighter-selected');
    }
}

export default FighterView;