import { callApi, callApiUpdate } from '../helpers/apiHelper';
// import Fighter from '../fighter';

class FighterService {

    fightersDetailsMap = new Map();

    async getFighters() {
        try {
            // const endpoint = 'fighters.json';
            const endpoint = 'user';
            const apiResult = await callApi(endpoint, 'GET');

            return apiResult.sort((e1, e2) => e1.id - e2.id);
        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(id) {
        const endpoint = `user/${id}`;

        try {

            if (!this.fightersDetailsMap.has(id)) {
                const fighter = await callApi(endpoint, 'GET');
                this.fightersDetailsMap.set(fighter.id, fighter);
            }
            return this.fightersDetailsMap.get(id);

        } catch (error) {
            throw error;
        }
    }

    async updateFighterDetail(fighter) {
        const endpoint = `user/${fighter.id}`;
        
        try {
            const result = await callApiUpdate(endpoint, fighter);
            if (result == true) {
                this.fightersDetailsMap.set(fighter.id, fighter);
                console.log('fighter info updated');
            }
            return result;
        }
        catch (error) {
            console.error(error);
            return false;
        }        
    }
}

export const fighterService = new FighterService();
