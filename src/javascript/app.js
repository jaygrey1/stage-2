import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import RingView from './ringView';

class App {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root');
    static selectStageElement = document.getElementById('select-stage');
    static fightStageElement = document.getElementById('fight-stage');
    static loadingElement = document.getElementById('loading-overlay');

    async startApp() {
        try {
            App.loadingElement.style.visibility = 'visible';

            const fighters = await fighterService.getFighters();
            this.fightersView = new FightersView(fighters, this.fightersSelectedHandler.bind(this));            
                        
            App.selectStageElement.appendChild(this.fightersView.element);
            App.selectStageElement.appendChild(this.fightersView.fightButton);
            
            this.ringView = new RingView(this.fightDoneHandler.bind(this));
            App.fightStageElement.appendChild(this.ringView.element);
            
            // App.rootElement.appendChild(this.selectStageElement);
            // App.rootElement.appendChild(this.selectStageElement);

        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            App.loadingElement.style.visibility = 'hidden';
        }
    }

    fightersSelectedHandler(fighters) {
        App.selectStageElement.style.display = 'none';
        
        this.ringView.setLeftFighter(fighters[0]);
        this.ringView.setRightFighter(fighters[1]);
        this.ringView.clearStage();        
        this.ringView.fight(fighters[0], fighters[1]);        
        App.fightStageElement.style.display = 'block';
    }

    fightDoneHandler() {
        App.fightStageElement.style.display = 'none';

        this.fightersView.clearStage();
        App.selectStageElement.style.display = 'block';
    }
}

export default App;