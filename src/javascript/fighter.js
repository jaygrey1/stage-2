class Fighter {
    id;
    name;
    health;
    attack;
    defense;

    constructor({_id, name, health, attack, defense, source}) {
        this.id = _id;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.image = source;
    }

    getHitPower() {
        let criticalHitChance = Math.random() < 0.5 ? 1 : 2;
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        let dodgeChance = Math.random() < 0.5 ? 1 : 2;
        return this.defense * dodgeChance;
    }
}

export default Fighter;