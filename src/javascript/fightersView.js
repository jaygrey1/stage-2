import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import DetailView from './detailView';

class FightersView extends View {
    constructor(fighters, doneHandler) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.selectClick = this.handleFighterSelectClick.bind(this);
        this.fightClick = this.handleFightButtonClick.bind(this);
        this.doneHandler = doneHandler;
        this.createFighters(fighters);

        this.selectedFighters = new Set();
        this.DetailView = new DetailView();
        this.fightButton = this.createFightButton();
    }

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick, this.selectClick);
            return fighterView.element;
        });

        this.element = this.createElement({ tagName: 'div', className: 'fighters' });
        this.element.append(...fighterElements);
    }

    createFightButton() {
        const fightButton = this.createElement({ tagName: 'div', className: 'fight-button' });
        const fightImage = this.createElement({ tagName: 'img', className: 'fight-button-image', attributes: { src: './resources/fight.png' } });
        fightButton.append(fightImage);
        fightImage.addEventListener('click', () => this.fightClick());

        return fightButton;
    }

    clearStage() {
        this.selectedFighters.clear();
        document.querySelectorAll(".fighter-checkbox").forEach(cb => cb.checked = false);
        document.querySelectorAll(".fighter-selected").forEach(fs => fs.classList.remove('fighter-selected'));
    }


    async handleFighterClick(event, fighterView) {
        console.log('clicked');

        try {
            const fighter = await fighterService.getFighterDetails(fighterView.id);
            this.DetailView.show(fighter);
        } catch (error) {
            console.error(error);
        }
            
    }

    handleFighterSelectClick(event, fighter, fighterView) {
        if (this.selectedFighters.has(fighter)) {
            console.log(`fighter ${fighter.name} deselected`);
            fighterView.deselect();
            this.selectedFighters.delete(fighter);
        } else if (this.selectedFighters.size < 2) {
            console.log(`fighter ${fighter.name} selected`);
            fighterView.select();
            this.selectedFighters.add(fighter);
        } else {
            event.preventDefault();
        }
        event.stopPropagation();
    }

    handleFightButtonClick() {
        const fighters = [...this.selectedFighters];

        const args = [];
        fighterService.getFighterDetails(fighters[0].id)
            .then(fighter1 => {
                args.push(fighter1);
                return fighterService.getFighterDetails(fighters[1].id);
            })
            .then(fighter2 => {
                args.push(fighter2);
                return args;
            })
            .then(fighters => {
                this.doneHandler(fighters);
            });
    }
}

export default FightersView;