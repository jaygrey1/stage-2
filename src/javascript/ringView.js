import View from './view';
import RingFighterView from './ringFighterView';
import Fighter from './fighter';

class RingView extends View {

    ROUND_INTERVAL = 100;

    constructor(doneHandler) {
        super();
        this.element = this.createRing();
        this.doneHandler = doneHandler;
    }

    createRing() {
        const element = this.createElement({ tagName: 'div', className: 'ring', attributes: {} });
        this.leftFighterElement = new RingFighterView('ring-lfighter');
        this.rightFighterElement = new RingFighterView('ring-rfighter');
        this.battleLog = this.createElement({ tagName: 'div', className: 'ring-battlelog', attributes: {} });

        this.endButton = this.createElement({ tagName: 'button', className: 'endFight', attributes: { type: 'button' } });
        this.endButton.innerText = 'Back to Fighters';
        this.endButton.addEventListener('click', () => {
            this.endButton.style.visibility = 'hidden';
            this.doneHandler();
        });

        element.append(this.leftFighterElement.element, this.battleLog, this.rightFighterElement.element, this.endButton);
        // element.style.display = 'none';

        return element;
    }

    setLeftFighter(fighter) {
        this.leftFighter = fighter;
        this.leftFighterElement.setupFighter(fighter);
    }

    setRightFighter(fighter) {
        this.rightFighter = fighter;
        this.rightFighterElement.setupFighter(fighter);
    }

    addToBattleLog(message) {
        const element = this.createElement({ tagName: 'p', className: 'bl-message', attributes: {} });
        element.innerText = message;
        this.battleLog.append(element);
    }

    fight(fighter1, fighter2) {        

        let attacker = new Fighter({
            _id: fighter1.id,
            name: fighter1.name,
            health: fighter1.health,
            attack: fighter1.attack,
            defense: fighter1.defense
        });

        let defender = new Fighter({
            _id: fighter2.id,
            name: fighter2.name,
            health: fighter2.health,
            attack: fighter2.attack,
            defense: fighter2.defense
        });

        new Promise((resolve, reject) => {
            setTimeout(() => this.round(attacker, defender, resolve), this.ROUND_INTERVAL);
        }).then(winner => {
            const message = `fighter ${winner.name} is WINNER !!!`;
            console.log(message);
            this.addToBattleLog(message);
            this.endButton.style.visibility = 'visible';
        });
    }

    round(attacker, defender, resolve) {
        const hitpoints = Math.max(attacker.getHitPower() - defender.getBlockPower(), 0);
                
        console.log(`${attacker.name} hit ${defender.name} by ${hitpoints} points`);

        if (hitpoints > 0) {            
            defender.health -= hitpoints;
            if (defender.id == this.leftFighter.id) {
                this.leftFighterElement.updateHealthbar(Math.max(defender.health, 0));
            } else {
                this.rightFighterElement.updateHealthbar(Math.max(defender.health, 0));
            }
        }
        
        if (defender.health > 0) {
            setTimeout(() => this.round(defender, attacker, resolve), this.ROUND_INTERVAL);
        } else {
            resolve(attacker);
        }
    }

    clearStage() {        
        while(this.battleLog.firstChild != null) {
            this.battleLog.removeChild(this.battleLog.firstChild);
        }

        this.leftFighterElement

    }
}

export default RingView;